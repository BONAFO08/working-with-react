import React from "react"
import './App.css';

import waifus from "./testData/waifu.json";
import Show from "./components/Show";

// 2:00:00 https://www.youtube.com/watch?v=zIY87vU33aA
// const Waifuname = (props) => {
//   return (
//     <div>
//       こにちわあ : {props.waifu}
//     </div>
//   )
// }

// class Spirits extends React.Component {
// state = {
//   show: true,
//   waifu : ''
// }

//   newWaifu = (waifu2)=>{
//     let newWaifu = prompt('New Waifu?');
//     this.setState({waifu :  newWaifu});
//   }
//   render() {
//     if(this.state.show && this.props.type === 'diosa'){
//       return (
//         <div>
//           <button onClick={()=>{this.newWaifu('Veru')}}>Change waifu</button>
//           こにちわあ : {this.state.waifu}様 {this.props.hola}
//         </div>
//       )
//     }else{
//       return (
//         <div>
//           こにちわあ : {this.props.waifu} {this.props.hola}
//         </div>
//       )
//     }

//   }
// }

// function App() {
//   return (
//     <div>
//       {
//       /* <Waifuname waifu='ベルベル' />
//       <Spirits waifu='十香' hola='元気ですか？' type = 'spirit'/>
//       <Spirits waifu='ノワール' hola='元気ですか？' type = 'diosa'/> */}
//     </div>
//   );
// }

class App extends React.Component {
  state = {
    waifus: waifus
  }

  render() {
    return <div key={'principalDiv'}>
      <Show waifus={this.state.waifus} />
    </div>

  }
}

export default App;

