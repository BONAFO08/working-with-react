import React, { Component } from "react";
import "../styles/Waifus.css"

class Waifus extends Component {
    render() {
        const {waifus} = this.props;
        return <div key={`divWaifu${waifus.id}`} style ={waifus.style} >
                <h3 key={`name${waifus.id}`}>{waifus.name}</h3>
                <h3 key={`faction${waifus.id}`}>{waifus.faction}</h3>
                ----------------
        </div>
    }
}

export default Waifus;
