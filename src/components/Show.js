import React, { Component } from "react";
import Waifus from "./Waifus";

class Show extends Component {
    render() {

        return this.props.waifus.map(e => <Waifus waifus = {e}/>)
        
    }
}

export default Show;
